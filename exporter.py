import arcpy

# Set to the location where all the output GDBs will be stored
GDB_LOCATION = r"D:\\work\\src\\experiments\\enterprise-gdb-exporter\\outputs\\"

# Set to location of a previously created connection file for the DB
arcpy.env.workspace = (r"""C:\\Users\\jmccarth\\Documents\\ArcGIS\\"""
                        r"""Projects\\MyProject3\\env-dbms-srv2.sde""")

def generate_file_gdbs():
    """lists the feature classes, feature datasets, and tables in the
    geodatabase, pulls out all of the unique schema names, and generates
    a file GDB for each schema in GDB_LOCATION"""
    fcs = arcpy.ListFeatureClasses()
    fds = arcpy.ListDatasets()
    tbls = arcpy.ListTables()

    schemas = []

    for fc in fcs:
        schemas.append(fc.split('.')[1])

    for fd in fds:
        schemas.append(fd.split('.')[1])

    for tbl in tbls:
        schemas.append(tbl.split('.')[1])

    # Get a unique list of usernames to create FGDBs
    usernames = set(schemas)

    # Create a file GDB for each schema (including the class schema)
    for uname in usernames:
        # Create an FGDB for the schema
        try:
            print("creating fgdb for {user}...".format(user=uname))
            arcpy.CreateFileGDB_management(GDB_LOCATION, uname)
        except arcpy.ExecuteError as err:
            print("problem creating fgdb for {user}: {error}".format(
                user=uname,
                error=err))
        else:
            print("fgdb for {user} created successfully".format(user=uname))

def copy_items():
    """Walks the database and copies selected items into existing file GDBs"""
    walk = arcpy.da.Walk(arcpy.env.workspace)

    for dirpath, dirnames, filenames in walk:
        for filename in filenames:
            # get username
            schema = filename.split('.')[1]

            # get user's GDB (it should have been created above)
            destination_gdb = r"{gdb_path}\\{user}.gdb".format(
                gdb_path=GDB_LOCATION,
                user=schema
            )

            # name of output file in GDB will match original file
            output_file = r"{destination_gdb}\\{filename}".format(
                destination_gdb=destination_gdb,
                filename=filename
            )

            # use describe object to get type of dataset
            datatype = arcpy.Describe(filename).dataType

            # only copy tables and feature classes (for now)
            # Note that relationship classes are automatically copied by
            # Copy_management when a table/FC that has one is copied
            if datatype == 'Table' or datatype == 'FeatureClass':
                try:
                    arcpy.Copy_management(filename, output_file)
                except arcpy.ExecuteError as err:
                    print("problem copying {filename}: {error}".format(
                        filename=filename,
                        error=err
                    ))

            print("{filename} is a {datatype}".format(
                filename=filename,
                datatype=arcpy.Describe(filename).dataType
            ))

generate_file_gdbs()
copy_items()
